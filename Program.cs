﻿using System;
using System.Collections.Generic;

namespace Assessment
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            /************************
            - Create list of Students
            - Create list of Teachers
            - Create list of Subjects
            ************************/
            List<Student>      students = new List<Student>();
            List<Teacher>      teachers = new List<Teacher>();
            List<Subject>      subjects = new List<Subject>();

            // Set up Subjects
            subjects.Add(new Subject("WIZ-5001", "Introduction to Offensive Spells: Fire Magic", "Leonard Embermane"));
            subjects.Add(new Subject("MAG-5002", "Magic Rituals for the Uninitiated", "Dorris Doomsayer"));
            subjects.Add(new Subject("WIZ-5002", "Counter-Magic, the Basics", "Tayren Aetherguy"));
            subjects.Add(new Subject("WLK-5001", "Curses 101", "Guul Dahn"));
            subjects.Add(new Subject("MAG-5001", "Basic Mana Control", "Winston Doomaxe"));

            // Set up Students
            students.Add(new Student("Hodor", students.Count, "Hodor Hodor"));
            students.Add(new Student("doge", students.Count, "Gerard Encausse"));
            students.Add(new Student("x_witch_slayer_x", students.Count, "Cornelius Agrippa"));
            students.Add(new Student("1337guy", students.Count, "Eliphas Levi"));
            students.Add(new Student("The Franz", students.Count, "Franz Bardon"));
            // Set up Teachers
            teachers.Add(new Teacher("Leonard_Embermane", teachers.Count, "Leonard Embermane"));
            teachers.Add(new Teacher("Dorris_Doomsayer", teachers.Count, "Dorris Doomsayer"));
            teachers.Add(new Teacher("Tayren_Aetherguy", teachers.Count, "Tayren Aetherguy"));
            teachers.Add(new Teacher("Guul_Dahn", teachers.Count, "Guul Dahn"));
            teachers.Add(new Teacher("Winston_Doomaxe", teachers.Count, "Winston Doomaxe"));
            teachers.Add(new Teacher("Guy_Adonis", teachers.Count, "Guy Adonis"));
            teachers.Add(new Teacher("Wright_Denholm", teachers.Count, "Wright Denholm"));
            teachers.Add(new Teacher("Harlot_Mallor", teachers.Count, "Harlot Mallor"));


            /************************
            You are then required to create a series of loops that displays different sets of data
            - The first loop should display what the teachers are teaching
            - The second loop should display what subjects the students are taking

            The idea for the first loop is that it checks whether or not you have assigned a teacher to a subject 
            so it will only print out the teacher / subject combination that are entered into a list.
             
            The second loop requires you to print out all the subjects that the student is taking and tells us what the name of that student is.
            ************************/


            // Loop 1 - What subjects are the teachers teaching?
            for (int i = 0; i < teachers.Count; i++)
            {
                Console.WriteLine(teachers[0].WhatAmITeaching(ref subjects, teachers[i].pName));
            }

            // Loop 2 - What subjects are students studying?
            for (int i = 0; i < students.Count; i++)
            {
                Console.WriteLine(students[i].ListAllMySubjects(ref subjects));
            }
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
